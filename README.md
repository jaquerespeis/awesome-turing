# Awesome Alan Turing

A curated list of awesome resources from and about [Alan Turing](https://en.wikipedia.org/wiki/Alan_Turing).

* Papers
  * [The Colossus](http://homepages.cs.ncl.ac.uk/brian.randell/Papers-Books/133.pdf), by [Brian Randell](https://en.wikipedia.org/wiki/Brian_Randell): "The paper also attempts to assess Turing’s role in the COLOSSUS story".

* Books
  * Biographies
    * [Alan Turing: The Enigma](https://en.wikipedia.org/wiki/Alan_Turing:_The_Enigma), by [Andrew Hodges](https://en.wikipedia.org/wiki/Andrew_Hodges)
 
  * Non-fiction
    * [The Turing Guide](https://en.wikipedia.org/wiki/The_Turing_Guide)
    * [The Annotated Turing](https://en.wikipedia.org/wiki/The_Annotated_Turing), by [Charles Petzold](https://en.wikipedia.org/wiki/Charles_Petzold)
  
  * Fiction
    * [Cryptonomicon](https://en.wikipedia.org/wiki/Cryptonomicon), by [Neal Stephenson](https://en.wikipedia.org/wiki/Neal_Stephenson)

* Movies
  * [The Imitation Game](https://en.wikipedia.org/wiki/The_Imitation_Game) ([fictionalized](https://en.wikipedia.org/wiki/The_Imitation_Game#Historical_inaccuracies))
 
* Other awesome resources
  * [The Turing Digital Archive](http://turingarchive.org/)
